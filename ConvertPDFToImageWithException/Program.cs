﻿using System.Drawing;
using System.Drawing.Imaging;
using Spire.Pdf;


namespace ConvertPDFToImageWithException
{
	class Program
	{
		static void Main(string[] args)
		{
			PdfDocument doc = new PdfDocument();
			doc.LoadFromFile("Alice_in_Wonderland-First-Page.pdf");
			Image imageMetafilePng = doc.SaveAsImage(0, Spire.Pdf.Graphics.PdfImageType.Metafile);
			imageMetafilePng.Save("convertTobitmap.png", ImageFormat.Png);
		}
	}
}
