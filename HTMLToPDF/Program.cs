﻿using System;
using System.Drawing;
using System.Threading;
using Spire.Pdf;
using Spire.Pdf.HtmlConverter;

namespace HTMLToPDF
{
	class Program
	{
		static void Main(string[] args)
		{
			PdfDocument doc = new PdfDocument();

			PdfPageSettings setting = new PdfPageSettings();

			setting.Size = new SizeF(1000, 1000);
			setting.Margins = new Spire.Pdf.Graphics.PdfMargins(20);

			PdfHtmlLayoutFormat htmlLayoutFormat = new PdfHtmlLayoutFormat();
			htmlLayoutFormat.IsWaiting = true;

			String url = "https://www.wikipedia.org/";

			Thread thread = new Thread(() =>
				{ doc.LoadFromHTML(url, true, true, true, setting, htmlLayoutFormat); });
			thread.SetApartmentState(ApartmentState.STA);
			thread.Start();
			thread.Join();
			doc.SaveToFile("output-wiki.pdf");
			doc.Close();
		}
	}
}
