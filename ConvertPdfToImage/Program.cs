﻿using System.Drawing;
using System.Drawing.Imaging;
using Spire.Pdf;
using Spire.Pdf.Graphics;

namespace ConvertPdfToImage
{
	class Program
	{
		static void Main(string[] args)
		{
			PdfDocument newPdfDocument = new PdfDocument();
			newPdfDocument.LoadFromFile("Alice_in_Wonderland-First-Page.pdf");
			Image bitmapImage = newPdfDocument.SaveAsImage(0, PdfImageType.Bitmap);
			bitmapImage.Save("convertTobitmap.bmp", ImageFormat.Bmp);
		}
	}
}
