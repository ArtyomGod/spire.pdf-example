﻿using System;
using System.Text;
using System.IO;
using Spire.Pdf;


namespace PDFToText
{
	class Program
	{
		static void Main(string[] args)
		{
			PdfDocument document = new PdfDocument();
			document.LoadFromFile(@"Alice_in_Wonderland-First-Page.pdf");
			StringBuilder content = new StringBuilder();
			content.Append(document.Pages[0].ExtractText());
			String fileName = "TextFromPDF.txt";
			File.WriteAllText(fileName, content.ToString());
			System.Diagnostics.Process.Start("TextFromPDF.txt");
		}
	}
}